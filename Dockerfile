FROM ros:kinetic-perception

RUN apt-get update && \
    apt-get install apt-utils usbutils -y

## OpenCV
ARG INSTALL_DIR=/opt/intel
RUN mkdir -p $INSTALL_DIR 
WORKDIR $INSTALL_DIR
RUN git clone https://github.com/opencv/opencv.git && \
    cd opencv && mkdir build && cd build
WORKDIR $INSTALL_DIR/opencv/build
RUN cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local .. 
RUN make -j4 && make install

## OpenVINO
WORKDIR $INSTALL_DIR
RUN cd $INSTALL_DIR && \
    git clone https://github.com/opencv/dldt.git && \
    cd $INSTALL_DIR/dldt/inference-engine && \
    git submodule init && \
    git submodule update --recursive 
RUN sh /opt/intel/dldt/inference-engine/install_dependencies.sh
ENV OpenCV_DIR=/usr/local/opencv4
WORKDIR $INSTALL_DIR/dldt/inference-engine
RUN mkdir build && cd build && \
    cmake -DCMAKE_BUILD_TYPE=Release \
      -DENABLE_MKL_DNN=OFF \
      -DENABLE_CLDNN=OFF \
      -DENABLE_GNA=OFF \
      -DENABLE_SSE42=OFF \
      -DTHREADING=SEQ \
      .. && \
      make -j4   

#RUN echo "source $INSTALL_DIR/bin/setupvars.sh">>/root/.bashrc
#

### OpenVino USB rules
ARG INSTALL_DIR=/opt/intel/dldt
SHELL ["/bin/bash", "-c"] 

#RUN sudo usermod -a -G users "$(whoami)" && \
#    $INSTALL_DIR/install_dependencies/install_NCS_udev_rules.sh
#

RUN sudo apt-get install -y --no-install-recommends \
    gdb \
    nano \
    vim

RUN echo "export InferenceEngine_DIR=$INSTALL_DIR/inference-engine/build">>/root/.bashrc
RUN mkdir $INSTALL_DIR/inference-engine/samples/build
WORKDIR $INSTALL_DIR/inference-engine/samples/build
RUN InferenceEngine_DIR=$INSTALL_DIR/inference-engine/build cmake .. && make -j4

WORKDIR /opt/intel/dldt/inference-engine/samples/build/armv7l/Release
