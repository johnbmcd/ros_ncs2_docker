# ROS Perception + NCS2 docker container

This repository contains setup files for a minimal ROS perception + NCS2 docker container to demonstrate an issue with access the Myriad X / NCS2 usb stick from within a docker container. The container is built from [dldt](https://github.com/opencv/dldt/) for compatibility with rasbpi 4. 

## Building the docker image
To build the repository run the following command:

`./build.sh`

This will build the container including the code samples within the dldt/inference-engine/samples folder.

## Running the docker image
The repo contains three scripts to run containers from the resultant docker image

`run` -- runs a bash shell within an interactive container

`run_hello_query_device` -- runs the hello_query_device sample

`run_hello_classification` -- attempls to run the hello_classification sample on the Myriad, using the person-detection-retail-0013-fp16 network model, passing image.jpg in the FP16 folder as input
